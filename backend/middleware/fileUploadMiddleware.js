import multer from 'multer';
import fs from 'fs';
import express from 'express'
import path from 'path'
import { protect } from './authMiddleware.js';
const imageUploadRouter = express.Router()

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
  	var dir = `uploads/${req.user.email}`;
	if (!fs.existsSync(dir)){
	    fs.mkdirSync(dir);
	}
    cb(null, `uploads/${req.user.email}/`)
  },
  filename: function (req, file, cb) {
    let fileDetails = file.mimetype.split('/');
    cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`);
  }
});

const checkFileType = (file, cb) => {
  const filetypes = /pg|jpeg|png/
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase())
  // const mimetype = filetypes.test(file.mimetype)
  // let extname = filetypes.testpath();
  if (extname) {
    return cb(null, true)
  } else {
    cb('Images only!')
  }
}

var upload = multer({ storage: storage, fileFilter: function(_req, file, cb)
  {
    checkFileType(file, cb); 
  }});

imageUploadRouter.use(protect);
imageUploadRouter.post('/', upload.single('postImage'), (req, res) => {
  res.send(`/${req.file.path}`)
})

export default imageUploadRouter