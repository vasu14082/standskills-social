import mongoose from 'mongoose';
import Comment from './commentModel.js';

const postSchema = mongoose.Schema({
	user:{
		type: mongoose.Schema.Types.ObjectId,
		required:true,
		ref:'User'
	},
	postName:{
		type:String,
		required:true
	},
	image:{
		type:String,
		required:true
	},
	description:{
		type:String,
		required:true
	},
	comments:[{
		type: mongoose.Schema.Types.ObjectId,
		ref:'Comment'
	}],
	likes:[{
		type: mongoose.Schema.Types.ObjectId,
		ref:'Like'
	}],
	numLikes:{
		type:Number,
		default:0,
	},
	numView:{
		type:Number,
		default:0,
	},
},{
	timestamps:true
});

const Post =  mongoose.model('Post', postSchema);

export default Post;