import bcrypt from 'bcryptjs';

const users = [
  {
    firstName: 'Admin',
    lastName: 'Admin',
    email: 'admin@example.com',
    password: bcrypt.hashSync('123456', 10),
    isAdmin: true,
  },
  {
    firstName:'vasu',
    lastName:'sharma',
    email: 'vasu14082@gmail.com',
    password: bcrypt.hashSync('123456', 10),
  },
]

export default users;