import asyncHandler from 'express-async-handler';
import User from '../models/userModel.js';
import jwt from 'jsonwebtoken';
import nodemailer from 'nodemailer';
import generateToken from '../utils/generateToken.js';

// @desc    Auth user
// @route   post /api/users/login
// @access  Private
const authUser = asyncHandler(async (req, res) => {

  const { email, password } = req.body;

  const user = await User.findOne({email});
  
  if(user && (await user.matchPassword(password))){
  	res.status(200).json({
  		_id:user._id,
  		name:user.name,
  		email:user.email,
  		isAdmin:user.isAdmin,
  		token:generateToken(user._id),
  	});
  	// res.end();
  } else {
  	res.status(401);
  	throw new Error('Invalid email or passworkd')
  }

  // res.send({email, password});
});

var email;
const getPassword = () =>
{
  let token = jwt.verify(process.env.PASSWORD, process.env.JWT_SECRET);
  return token.password;
}

const getOtpWithTimeLimit = () =>{
  let otp = Math.random();
  otp = otp * 1000000;
  otp = parseInt(otp);
  let currentTimme = Date.now();
  global.otpData = {"otp": otp, "otpgeneratedTime": currentTimme};
  return;
}


// @desc    Register a new user
// @route   POST /api/users
// @access  Public
const registerUser = asyncHandler(async (req, res) => {
  global.userData = req.body;
  // console.log(global);
  // return res.status(200).json({message:"this is vasu"});
  let otpObject = getOtpWithTimeLimit();
  let transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 465,
      secure: true,
      service : 'Gmail',

      auth: {
        user: process.env.EMAIL,
        pass: getPassword(),
      } 

  });

  const { firstName, lastName, email, password } = req.body

  const userExists = await User.findOne({ email })

  if (userExists) {
    res.status(400)
    throw new Error('User already exists')
  }

  // send mail with defined transport object
  var mailOptions={
    to: email,
    subject: "Otp for registration in standskills: ",
    html: "<h3>OTP for account verification is </h3>"  + "<h1 style='font-weight:bold;'>" + global.otpData.otp +"</h1>" // html body
   };

  transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
          res.status(401);
          throw new Error("Something Went wrong");
      }
      console.log('Message sent: %s', info.messageId);   
      console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
  });


  res.status(200).json({
    message:"please enter otp that sent you in email for registrations"
  });
})

// @desc    Verify new user
// @route   POST /api/users/verify
// @access  Public
const verifyOtp = asyncHandler(async (req, res) => {
  const otp = req.body.otp;
  console.log(global.otpData);
  if((global.otpData.otpgeneratedTime - Date.now()) > (process.env.TIME_LIMIT*60000))
  {
    res.status(401);
    throw new Error("Invalid otp");
  }
  const { firstName, lastName, email, password } = global.userData;

  if(otp === global.otpData.otp)
  {
    const user = await User.create({
      firstName,
      lastName,
      email,
      password,
    })

    if (user) {
      res.status(201).json({
        _id: user._id,
        name: user.name,
        email: user.email,
        isAdmin: user.isAdmin,
        token: generateToken(user._id),
      })
    } else {
      res.status(400)
      throw new Error('Invalid user data')
    }
  } else {
    res.status(500);
    throw new Error('Invalid otp');
  }
});

// @desc    Get user profile
// @route   GET /api/users/profile
// @access  Private
const getUserProfile = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user._id)

  if (user) {
    res.json({
      _id: user._id,
      firstName: user.firstName,
      lastName:user.lastName,
      email: user.email,
      isAdmin: user.isAdmin,
    })
  } else {
    res.status(404)
    throw new Error('User not found')
  }
})

// @desc    update user profile
// @route   PUT /api/users/profile
// @access  Private
const updateUserProfile = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user._id)

  if(req.body.email && user.email != req.body.email)
  {
    res.status(422);
    throw new Error("Email field can't be updated");
  }
  if(user)
  {
    user.firstName = req.body.firstName || user.firstName;
    user.lastName = req.body.lastName || user.lastName;
    if(req.body.password)
    {
      user.password = req.body.password;
    }

    const updatedUser = await user.save();

    res.json({
       _id: updatedUser._id,
      firstName: updatedUser.firstName,
      lastName: updatedUser.lastName,
      email: updatedUser.email,
      isAdmin: updatedUser.isAdmin,
      token: generateToken(updatedUser._id),
    })
  } else {
    res.status(404)
    throw new Error('User not found')
  }
})

export { registerUser, verifyOtp, authUser, getUserProfile, updateUserProfile }