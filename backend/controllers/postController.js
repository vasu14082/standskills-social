import asyncHandler from 'express-async-handler';
import Post from '../models/postModel.js';
import fs from 'fs';
import uploadImage from '../middleware/fileUploadMiddleware.js';
import path from 'path';

// @desc    Get user posts
// @route   GET /api/posts
// @access  Private
const getUserPosts = asyncHandler(async (req, res) => {
  const post = await Post.find({user:req.user._id}).populate('user', 'firstName lastName email createdAt updatedAt')

  if (post) {
    return res.json(post);
  } else {
    res.status(404)
    throw new Error('User not found')
  }
})

// @desc    Get user posts
// @route   POST /api/posts
// @access  Private
const createPost = asyncHandler(async (req, res) => {
	var dir = `uploads/${req.user.email}`;
	if (!fs.existsSync(dir)){
	    fs.mkdirSync(dir);
	}
	if(!(req.files && req.files.postImage))
	{
		res.status(422);
		throw new Error("Imge file is compulsory");
	}
	const filetypes = /pg|jpeg|png/;
	const extname = filetypes.test(path.extname(req.files.postImage.name).toLowerCase())
	if(!extname)
	{
		res.status(422);
		throw new Error("only image");
	}
	const FILENAME = `image-${Date.now()}${path.extname(req.files.postImage.name)}`;
	req.files.postImage.mv(`uploads/${req.user.email}/${FILENAME}`,(err) => {
		if(err){
			res.status(422);
			throw new Error("Something went wrong wile uploading");
		}
		return true;
	});
	const IMAGEPATH = `/uploads/${req.user.email}/${FILENAME}`;
	const { postName, description } = req.body;
	const post = await Post.create({
		user:req.user._id,
		postName,
		image:IMAGEPATH,
		description
	});
	if(post)
	{
		res.status(201).json({
			_id: post._id,
			postName: post.postName,
			image: post.image,
			description: post.description,
			numLikes:post.numLikes,
			numView:post.numView,
		})
	} else {
      res.status(400)
      throw new Error('Invalid user data')
    }
});

// @desc    update user profile
// @route   PUT /api/users/profile
// @access  Private
const updatePost = asyncHandler(async (req, res) => {
  const post = await Post.findById(req.body.postId);

  if(req.body.numLikes || req.body.numView)
  {
    res.status(422);
    throw new Error("This field can't be updated");
  }
  if(post)
  {
    post.postName = req.body.postName || post.postName;
    post.description = req.body.description || post.description;

    const updatedPost = await post.save();

    res.json({
      	postName: updatedPost.postName,
		image: updatedPost.image,
		description: updatedPost.description,
		numLikes:updatedPost.numLikes,
		numView:updatedPost.numView,
    })
  } else {
    res.status(404)
    throw new Error('User not found')
  }
})

// @desc    update user profile
// @route   PUT /api/users/profile
// @access  Private
const updatePostLike = asyncHandler(async(req, res) => {
	const post = await Post.findById(req.body.postId);
	if(post)
	{
		post.numLikes = post.numLikes + 1;
		const updatedPost = await post.save();
		if(updatedPost)
		{
			res.json({
		      	postName: updatedPost.postName,
				image: updatedPost.image,
				description: updatedPost.description,
				numLikes:updatedPost.numLikes,
				numView:updatedPost.numView,
		    })
		} else {
			res.status(422);
    		throw new Error("like is not updated yet");
		}
	} else {
      res.status(404)
      throw new Error('Post not found')
    }

});

// @desc    update user profile
// @route   PUT /api/users/profile
// @access  Private
const updatePostView = asyncHandler(async(req, res) => {
	const post = await Post.findById(req.body.postId);
	if(post)
	{
		post.numView = post.numView + 1;
		const updatedPost = await post.save();
		if(updatedPost)
		{
			res.json({
		      	postName: updatedPost.postName,
				image: updatedPost.image,
				description: updatedPost.description,
				numLikes:updatedPost.numLikes,
				numView:updatedPost.numView,
		    })
		} else {
			res.status(422);
    		throw new Error("like is not updated yet");
		}
	} else {
      res.status(404)
      throw new Error('Post not found')
    }

});

export { getUserPosts, createPost, updatePost, updatePostView, updatePostLike }