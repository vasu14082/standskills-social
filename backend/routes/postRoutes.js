import express from 'express';
import { getUserPosts, createPost,updatePostLike, updatePostView, updatePost } from '../controllers/postController.js';
import { protect } from '../middleware/authMiddleware.js';

const postRoutes = express.Router();

postRoutes.route('/').get(protect, getUserPosts).post(protect, createPost).put(protect, updatePost);
postRoutes.route('/likes').put(protect, updatePostLike);
postRoutes.route('/views').put(protect, updatePostView);

export default postRoutes;