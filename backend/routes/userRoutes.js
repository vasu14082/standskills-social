import express from 'express';
import { registerUser, verifyOtp, authUser, getUserProfile, updateUserProfile } from '../controllers/userController.js';
import { protect } from '../middleware/authMiddleware.js';

const userRoutes = express.Router();

userRoutes.route('/').post(registerUser);
userRoutes.route('/verify').post(verifyOtp);
userRoutes.route('/login').post(authUser);
userRoutes.route('/profile').get(protect, getUserProfile).put(protect, updateUserProfile);

export default userRoutes;