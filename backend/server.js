import express from 'express';
import dotenv from 'dotenv';
import { notFound, errorHandler } from './middleware/errorMiddleware.js';
import connectDB from './config/db.js'
import userRoutes from './routes/userRoutes.js';
import postRoutes from './routes/postRoutes.js';
import imageRoutes from './middleware/fileUploadMiddleware.js';
import path from 'path';
import fileUpload  from 'express-fileupload';

dotenv.config();
connectDB();
const app = express();
app.use(express.json());
app.use(fileUpload());

app.get('/', (req, res) => {
	res.send('API is running...');
});	

app.use('/api/users', userRoutes);
app.use('/api/posts', postRoutes);
app.use('/api/upload', imageRoutes);

const __dirname = path.resolve();
app.use('/uploads', express.static(path.join(__dirname, '/uploads')));

app.use(notFound)
app.use(errorHandler)
const PORT = process.env.PORT || 5000;


app.listen(5000,
	console.log(`server running in ${process.env.NODE_ENV} mode on port ${PORT}`
	)
);