import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';


dotenv.config();

// console.log(jwt.verify(token, process.env.JWT_SECRET));

if(process.argv[2] === '-d')
{
	const token = process.argv[3];
	console.log(jwt.verify(token, process.env.JWT_SECRET));	
}
else
{
	const token = jwt.sign({password:process.argv[2]}, process.env.JWT_SECRET, {expiresIn:'30d'})
	console.log(token);
}