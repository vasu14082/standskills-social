import React, { useEffect, useState } from 'react';
import { Container, Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
const Header = (props) => {
	const [token, setToken] = useState(0);
	useEffect(() => {
		let tk = localStorage.getItem('TOKEN') ? JSON.parse(localStorage.getItem('TOKEN')) : 0;
		// console.log("tk", tk);

		if(tk){
			// console.log("tk in condition", tk);
			setToken(tk);
		}
	}, [token]);

	const logoutHandler = () => {
		localStorage.removeItem('TOKEN');
		setToken(0);
		window.location.href = '/login';
	}
	return (
		<header>
			<Navbar bg='dark' variant='dark' expand='lg' collapseOnSelect>
				<Container>
				  <LinkContainer to='/'>
				  	<Navbar.Brand>Stand Skills</Navbar.Brand>
				  </LinkContainer>
				  <Navbar.Toggle aria-controls="basic-navbar-nav" />
				  <Navbar.Collapse id="basic-navbar-nav">
				    <Nav className="ml-auto">
				    { token ? (
						<NavDropdown title={<React.Fragment><i className="fas fa-user"></i>User</React.Fragment>} id='username'>
		                  <LinkContainer to='/profile'>
		                    <NavDropdown.Item>Profile</NavDropdown.Item>
		                  </LinkContainer>
		                  <NavDropdown.Item onClick={logoutHandler}>
		                    Logout
		                  </NavDropdown.Item>
		                </NavDropdown>
				    ):(
				    	<LinkContainer to='/login'>
		                  <Nav.Link>
		                    <i className='fas fa-user'></i> Sign In
		                  </Nav.Link>
		                </LinkContainer>
		             )
				    }
				    </Nav>
				  </Navbar.Collapse>
				 </Container>
			</Navbar>
		</header>
	);
}

export default Header;