import React from 'react';
import { Link } from 'react-router-dom'
import { Card, Row, Col } from 'react-bootstrap';

const Post = ({post}) => {
	return(<Card className='my-3 p-3 rounded'>
			<Link to={`/post/${post._id}`}>
				<Card.Img src={post.image} variant='top' />
			</Link>

			<Card.Body>
				<Link to={`/post/${post._id}`}>
					<Card.Title as='div'>
						<strong>{post.name}</strong>
					</Card.Title>
				</Link>
					<Row>
						<Col md={4}>
							<i className="fas fa-eye">{post.numView}</i>
						</Col>
						<Col md={4}>
							<i className="far fa-heart">{post.numLikes}</i>
						</Col>
						<Col md={4}>
							<i className="far fa-comments">{post.comments.length}</i>
						</Col>
					</Row>
			</Card.Body>
		</Card>
	)
} 

export default Post