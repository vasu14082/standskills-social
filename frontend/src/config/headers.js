export const loginConfig = {
  headers: {
    'Content-Type': 'application/json',
  },
}

export const BearerConfig = () => {
	let token = localStorage.getItem('TOKEN') ? JSON.parse(localStorage.getItem('TOKEN')) : 0;
	return {headers: {
		'Content-Type': 'application/json',
		Authorization: `Bearer ${token}`,
	}};
}