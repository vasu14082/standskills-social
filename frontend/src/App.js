import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import Header from './components/Header.js';
import Footer from './components/Footer.js';
import HomeScreen from './screens/HomeScreen.js';
import LoginScreen from './screens/LoginScreen.js';
import RegisterScreen from './screens/RegisterScreen.js';
import VerifyScreen from './screens/VerifyScreen.js';
import ProfileScreen from './screens/ProfileScreen.js';
import AddPostScreen from './screens/AddPostScreen.js';
import './bootstrap.min.css';

function App() {
  return (
    <Router>
      <Header />
      <main className='py-3'>
        <Container>
          <Route path='/' component={HomeScreen} exact />
          <Route path='/login' component={LoginScreen} />
          <Route path='/register' component={RegisterScreen} />
          <Route path='/verify' component={VerifyScreen} />
          <Route path='/profile' component={ProfileScreen} />
          <Route path='/addpost' component={AddPostScreen} />
        </Container>
      </main>
      <Footer />
    </Router>
  );
}

export default App;
