import React from 'react';
import Post from '../components/Post.js';
import { posts } from '../data/data.js';
import { Row, Col, Container } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
const HomeScreen = () => {
	return(
		<Container>
			<Row>
				<Col md={6}>
					<h1>Latest Posts</h1>
				</Col>
				<Col md={6}>
					<LinkContainer to='/addpost'>
						<i className="far fa-plus-square fa-3x float-right"></i>
					</LinkContainer>
				</Col>
			</Row>
			<Row>
	            {posts.map((post) => (
	              <Col key={post._id} sm={12} md={6} lg={4} xl={3}>
	                <Post post={post} />
	              </Col>
	            ))}
            </Row>
		</Container>
	)

}

export default HomeScreen;