import React, { useState, useEffect } from 'react'
import { Table, Form, Button, Row, Col } from 'react-bootstrap'
import Message from '../components/Message'
import Loader from '../components/Loader'
import axios from 'axios'
import { BearerConfig } from '../config/headers.js';
import FormContainer from '../components/FormContainer'
import Post from '../components/Post.js';

const ProfileScreen = ({ location, history }) => {
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [message, setMessage] = useState(null)
  const [userDetails, setUserDetail] = useState({})
  const [success, setSuccess] = useState(false);
  const [loading, setLoading] = useState(true);
  const [posts, setPosts] = useState([]);
  const [postLoading, setPostLoading] = useState(true);
  const [postError, setPostError] = useState(null);

  useEffect(() => {
    let token = localStorage.getItem('TOKEN') ? JSON.parse(localStorage.getItem('TOKEN')) : 0;
    if (!token) {
      history.push('/login');
    } else {
      axios.get('/api/users/profile', BearerConfig())
      .then((response) => {
        setLoading(false)
        // console.log("success", response);
        setUserDetail(response.data);
        setFirstName(response.data.firstName);
        setLastName(response.data.lastName);
        setEmail(response.data.email);
      })
      .catch((err) => {
        // console.log("err", err);
        setMessage(err.response.data.message);
      })
      .then((response) =>{
        setLoading(false);
        // console.log("finally", response);
        axios.get('/api/posts', BearerConfig())
        .then((response) => {
          // console.log("success", response);
          setPostLoading(false);
          setPosts(response.data);
        })
        .catch((err) => {
          setPostError(err.response.data.message);
        })
        .then((response) => {
          setPostLoading(false);
        })
      })
    }
  }, [history])

  const submitHandler = (e) => {
    setLoading(true);
    e.preventDefault()
    if (password !== confirmPassword) {
      setMessage('Passwords do not match')
    }else {
      setUserDetail({firstName, lastName, email, password});
      axios.put('/api/users/profile', userDetails, BearerConfig())
      .then((response) => {
        setSuccess(true);
        // console.log("success", response);
      })
      .catch((err) => { 
        // console.log("err", err);
        setMessage(err.response.data.message);
      })
      .then((res) => {
        setLoading(false);
      })
    }
  }

  return (
    <Row>
      <Col md={3}>
        <h2>User Profile</h2>
        {message && <Message variant='danger'>{message}</Message>}
        {}
        {success && <Message variant='success'>Profile Updated</Message>}
        {loading ? (
          <Loader />
        ) :
        (
          <Form onSubmit={submitHandler}>
            <Form.Group controlId='firstName'>
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type='name'
                placeholder='Enter First Name'
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Form.Group controlId='lastName'>
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type='name'
                placeholder='Enter Last Name'
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Form.Group controlId='email'>
              <Form.Label>Email Address</Form.Label>
              <Form.Control
                readOnly={true}
                type='email'
                placeholder='Enter email'
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Form.Group controlId='password'>
              <Form.Label>Password</Form.Label>
              <Form.Control
                type='password'
                placeholder='Enter password'
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Form.Group controlId='confirmPassword'>
              <Form.Label>Confirm Password</Form.Label>
              <Form.Control
                type='password'
                placeholder='Confirm password'
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Button type='submit' variant='primary'>
              Update
            </Button>
          </Form>
        )}
      </Col>
      <Col md={9}>
        <h3>Your Posts </h3>
        <Row>
          {postError && <Message variant='danger'>{postError}</Message>}
          {postLoading ? <Loader /> : (
              posts.map((post) => (
                <Col key={post._id} sm={12} md={6} lg={4} xl={3}>
                  <Post post={post} />
                </Col>
              ))
            )
          }
        </Row>
      </Col>
    </Row>
  )
}

export default ProfileScreen