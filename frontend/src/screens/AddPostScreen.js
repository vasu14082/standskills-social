import React, { useState, useEffect } from 'react'
import { Form, Button, Row, Col } from 'react-bootstrap'
import Message from '../components/Message'
import FormContainer from '../components/FormContainer'
import axios from 'axios';

const AddPostScreen = ({ location, history }) => {
	// console.log(location, history);
	const [postName, setPostName] = useState('');
	const [description, setDescription] = useState('')
	const [error, setError] = useState(null);

	useEffect(() => {
		let token = localStorage.getItem('TOKEN') ? JSON.parse(localStorage.getItem('TOKEN')) : 0;
		if (!token) {
		  history.push('/login');
		}
	}, [history]);

	const uploadFileHandler = (e) => {
		e.preventDefault();
    console.log("File uploading", e);
	};

  const submitHandler = (e) => {
    console.log(e);
    e.preventDefault();
    const file = new FormData(this);
    console.log(file);
    // try {
    //   const config = {
    //     headers: {
    //       'Content-Type': 'multipart/form-data',
    //     },
    //   }


    // }
  }
	return (
	    <FormContainer>
	      <h1>New Post</h1>
	      {error && <Message variant='danger'>{error}</Message>}
	      <Form onSubmit={submitHandler}>
	        <Form.Group controlId='image'>
              <Form.Label>Image</Form.Label>
              <Form.File
                required
                id='image-file'
                label='Choose File'
                custom
                onChange={uploadFileHandler}
              ></Form.File>
            </Form.Group>
            <Form.Group controlId='postName'>
              <Form.Label>Post Name</Form.Label>
              <Form.Control
              	required
                type='text'
                placeholder='Enter Post Name'
                className="required"
                value={postName}
                onChange={(e) => setPostName(e.target.value)}
              ></Form.Control>
            </Form.Group>
            <Form.Group controlId="postDescription">
              <Form.Label>Post postDescription</Form.Label>
              <Form.Control as="textarea" rows={3}
                required
                type='text'
                placeholder='Enter Post postDescription'
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              ></Form.Control>
            </Form.Group>
	        <Button type='submit' variant='primary'>
	          Create Post
	        </Button>
	      </Form>
	    </FormContainer>
	  )
}

export default AddPostScreen;