import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Form, Button, Row, Col } from 'react-bootstrap'
import Message from '../components/Message'
// import Loader from '../components/Loader'
import FormContainer from '../components/FormContainer'
import axios from 'axios';
import { loginConfig } from '../config/headers.js';

const VerifyScreen = ({ location, history }) => {
	// console.log(location, history);
	const [otp, setOtp] = useState(null)
  	const [password, setPassword] = useState('')
  	const [error, setError] = useState(null);

	useEffect(() => {
		let token = localStorage.getItem('TOKEN') ? JSON.parse(localStorage.getItem('TOKEN')) : 0;
		if (token) {
		  history.push('/');
		}
	}, [history]);

	const submitHandler = (e) => {
		e.preventDefault();
		axios.post(
	      '/api/users/verify',
	      { "otp": Number.parseInt(otp) }
	    ).then((response) => {
	    	// console.log("success",response);
	    	localStorage.setItem("TOKEN", JSON.stringify(response.data.token));
	    	history.push('/');
	    	window.location.reload();
	    }
	    ).catch((error) => {//console.log("error", error, error.message)
	    	setError(error.message)
	    	// console.log("error", error.message);
	    }).then((response) => {
	    	// console.log("finally",response);
	    });
	};
	return (
	    <FormContainer>
	      <h1>Sign In</h1>
	      {error && <Message variant='danger'>{error}</Message>}
	      <Form onSubmit={submitHandler}>
	        <Form.Group controlId='email'>
	          <Form.Label>Enter OTP (check your email for otp)</Form.Label>
	          <Form.Control
	          	required
	            type='text'
	            placeholder='Enter OTP'
	            value={otp}
	            onChange={(e) => setOtp(e.target.value)}
	          ></Form.Control>
	        </Form.Group>
	        <Button type='submit' variant='primary'>
	          Register
	        </Button>
	      </Form>
	    </FormContainer>
	  )
}

export default VerifyScreen;