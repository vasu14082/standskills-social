import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Form, Button, Row, Col } from 'react-bootstrap'
import Message from '../components/Message'
// import Loader from '../components/Loader'
import FormContainer from '../components/FormContainer'
import axios from 'axios';
import { loginConfig } from '../config/headers.js';

const LoginScreen = ({ location, history }) => {
	// console.log(location, history);
	const [email, setEmail] = useState('')
  	const [password, setPassword] = useState('')
  	const [error, setError] = useState(null);

  	const redirect = location.search ? location.search.split('=')[1] : '/';

	useEffect(() => {
		let token = localStorage.getItem('TOKEN') ? JSON.parse(localStorage.getItem('TOKEN')) : 0;
		if (token) {
		  history.push(redirect);
		}
	}, [history, redirect]);

	const submitHandler = (e) => {
		e.preventDefault();
		axios.post(
	      '/api/users/login',
	      { email, password },
	      loginConfig
	    ).then((response) => {
	    	// console.log("success",response);
	    	localStorage.setItem("TOKEN", JSON.stringify(response.data.token));
	    	history.push(redirect);
	    	window.location.reload();
	    }
	    ).catch((error) => {//console.log("error", error, error.message)
	    	setError(error.response.data.message);
	    	// console.log("error", error.message);
	    }).then((response) => {
	    	// console.log("finally",response);
	    });
	};
	return (
	    <FormContainer>
	      <h1>Sign In</h1>
	      {error && <Message variant='danger'>{error}</Message>}
	      <Form onSubmit={submitHandler}>
	        <Form.Group controlId='email'>
	          <Form.Label>Email Address</Form.Label>
	          <Form.Control
	          	required
	            type='email'
	            placeholder='Enter email'
	            value={email}
	            onChange={(e) => setEmail(e.target.value)}
	          ></Form.Control>
	        </Form.Group>

	        <Form.Group controlId='password'>
	          <Form.Label>Password</Form.Label>
	          <Form.Control
	          	required
	            type='password'
	            placeholder='Enter password'
	            value={password}
	            onChange={(e) => setPassword(e.target.value)}
	          ></Form.Control>
	        </Form.Group>

	        <Button type='submit' variant='primary'>
	          Sign In
	        </Button>
	      </Form>

	      <Row className='py-3'>
	        <Col>
	          New Customer?{' '}
	          <Link to={redirect ? `/register?redirect=${redirect}` : '/register'}>
	            Register
	          </Link>
	        </Col>
	      </Row>
	    </FormContainer>
	  )
}

export default LoginScreen;